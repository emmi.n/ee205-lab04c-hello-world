///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
/// @file hello2.cpp
/// @version 1.0
///
/// A simple program to print "Hello WOrld!" using the g++ compiler
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 04c - Hello World - EE 205 - Spr 2021
///  @date   02_14_2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

int main() {
   
   std::cout << "Hello World!" << std::endl;

}
