///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
/// @file hello1.cpp
/// @version 1.0
///
/// A simple program to print "Hello WOrld!" using the g++ compiler
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 04c - Hello World - EE 205 - Spr 2021
///  @date   02_14_2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

using namespace std;

int main() {
   
   cout << "Hello World!" << endl;

}
