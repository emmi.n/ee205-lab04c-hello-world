# Build several interesting Hello World programs

TARGETS = hello1 hello2
CC = g++

all: $(TARGETS)

hello1: hello1.o
	$(CC) -o hello1 hello1.o

hello2: hello2.o
	$(CC) -o hello2 hello2.o

hello1.o: hello1.cpp
	$(CC) -c hello1.cpp

hello2.o: hello2.cpp
	$(CC) -c hello2.cpp

clean:
	rm -f $(TARGETS) *.o

